/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio5 {
    
    
    public static void toCamelProgram(){
        System.out.println("Por favor inserte una frase");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        String frase = teclado.nextLine();
        System.out.printf("La frase:\n%s\nen camelCase es:\n%s\n", frase, toCamelCase(frase));
    }
    
    private static String toCamelCaseDificil(String texto){
        //Quitamos los espacios al principio y al final y pasamos a minúsculas
        StringBuilder sb = new StringBuilder(texto.toLowerCase().trim()); 
        for(int i = 0; i < sb.length(); i++){
            char actual = sb.charAt(i);
            if(actual == ' '){
                //Tenemos que hacer esto por si hay más de un espacio junto. Si lo hay, borramos todos los espacios.
                do{
                    sb.deleteCharAt(i);
                }while(sb.charAt(i) == ' ');
                //Cuando borramos un caracter, el caracter inmediatamente a la derecha pasa a ser el caracter actual
                //Hola mundo
                //    ^
                //Holamundo
                //    ^
                sb.replace(i, i+1, sb.substring(i, i+1).toUpperCase());
            }            
        }
        return sb.toString();
    }
    
    private static String toCamelCase(String texto){
        StringBuilder sb = new StringBuilder("");
        texto = texto.toLowerCase().trim(); //Pasamos a minúsculas y quitamos los espacios que pueda haber al principio y al final 
        boolean spaceFound = false;
        for(int i = 0; i < texto.length(); i++){
            char actual = texto.charAt(i);
            if(actual == ' '){
                spaceFound = true;
            }
            else{
                if(spaceFound){
                    sb.append(Character.toUpperCase(actual));
                    spaceFound = false;
                }
                else{
                    sb.append(actual);
                }
            }
        }
        return sb.toString();
    }
}
