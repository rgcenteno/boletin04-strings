/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio4 {
    
    public static void esAnagramaProgram(){
        System.out.println("Por favor inserte dos palabras");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        String s1 = teclado.nextLine();
        System.out.println("Segunda palabra:");
        String s2 = teclado.nextLine();
        System.out.printf("La palabra \"%s\" es anagra de \"%s\": %b\n", s1, s2, esAnagrama(s1, s2));
    }
    
    private static boolean esAnagrama(String s1, String s2){
        if(s1.length() == s2.length()){
            StringBuilder sb2 = new StringBuilder(s2);
            for(int i = 0; i < s1.length(); i++){
                String actual = s1.substring(i, i + 1);
                int posicion = s2.indexOf(actual);
                if(posicion == -1){
                    return false;
                }
                else{
                    sb2.deleteCharAt(posicion);
                }
            }
            return true;
        }
        else{
            return false;
        }
    }
}
