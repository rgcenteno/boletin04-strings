/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

import java.util.Scanner;

/**
 *
 * @author rgcenteno
 */
public class Boletin04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String line;
        do{
            System.out.println("******************************************");
            System.out.println("* 1. Todas vocales                       *");
            System.out.println("* 2. Es palíndromo                       *");
            System.out.println("* 3. Es fecha                            *");
            System.out.println("* 4. Anagram                             *");
            System.out.println("* 5. To camelCase                        *");
            System.out.println("* 6. BracketMatcher                      *");
//            System.out.println("* 7. Calcular factorial                  *");
//            System.out.println("* 8. Fibonacci                           *");
//            System.out.println("* 9. Cálculo inversión                   *");
//            System.out.println("* 10. Números de Armstrong               *");
            System.out.println("*                                        *");
            System.out.println("* 0. Salir                               *");
            System.out.println("******************************************");
            line = teclado.nextLine();            
            switch(line){
                case "1":
                    Ejercicio1.contieneVocalesProgram();
                    break;  
                case "2":
                    Ejercicio2.esPalindromoProgram();
                    break;  
                case "3":
                    Ejercicio3.fechaCorrectaProgram();
                    break;  
                case "4":
                    Ejercicio4.esAnagramaProgram();
                    break;  
                case "5":
                    Ejercicio5.toCamelProgram();
                    break; 
                case "6":
                    Ejercicio6.bracketMatcherProgram();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción incorrecta!");
            }
        }while(!line.equals("0"));
      
    }
    
}
