/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio6 {
    
    public static void bracketMatcherProgram(){
        System.out.println("Por favor inserte texto a validar");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        String frase = teclado.nextLine();
        System.out.printf("En el texto:\n%s\nen los corchetes están bien definidos: %b\n", frase, bracketMatcher(frase));
    }
    
    private static boolean bracketMatcher(String entrada){
        int abiertos = 0;
        for(int i = 0; i < entrada.length(); i++){
            if(entrada.charAt(i) == '['){
                abiertos++;
            }
            else if(entrada.charAt(i) == ']'){
                abiertos--;
                if(abiertos < 0){
                    return false;
                }
            }
        }
        return abiertos == 0; //Sólo es true si hemos cerrado todos los que abrimos
    }
}
