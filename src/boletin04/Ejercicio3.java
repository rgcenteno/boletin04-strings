/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio3 {
    
    public static void fechaCorrectaProgram(){
        System.out.println("Por favor inserte una fecha en formato dd/mm/yyyy");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        String fecha = teclado.nextLine();
        System.out.printf("La fecha \"%s\" es válida: %b\n", fecha, esFecha(fecha));
    }
    
    private static boolean esFecha(String fecha){
        if(fecha.charAt(2) != '/' || fecha.charAt(5) != '/' || fecha.length() != 10){
            /*
            31/2/2020000
            */
            return false;
        }
        else{
        
            String diaString = fecha.substring(0, 2);
            String mesString = fecha.substring(3, 5);
            String anhoString = fecha.substring(6);        
            if(isNumericString(diaString) && isNumericString(mesString) && isNumericString(anhoString)){
                int dia = Integer.parseInt(diaString);
                int mes = Integer.parseInt(mesString);
                int anho = Integer.parseInt(anhoString);
                return (anho > 0) && (mes >= 1 && mes <= 12) && checkDia(dia, mes, anho);
            }
            else{
                return false;
            }
        }
    }
    
    private static boolean checkDia(int dia, int mes, int ano){
        if(dia >= 1 && dia <= 28){
            return true;
        }
        else if(dia <= 31){
            int limite = 0;
            if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
                limite = 31;
            }
            else if(mes == 2){
                if(esBisiesto(ano)){
                    limite = 29;
                }
                else{
                    limite = 28;
                }
                //limite = esBisiesto(ano) ? 29 : 28;
            }
            else{
                limite = 30;
            }
            return limite >= dia;
        }
        else{
            return false;
        }
    }
    
    private static boolean esBisiesto(int anho){
        return (anho % 4 == 0 && anho % 100 != 0) || anho % 400 == 0;
    }
    
    private static boolean isNumericString(String text){        
        for(int i = 0; i < text.length(); i++){
            char actual = text.charAt(i);
            if(!Character.isDigit(actual)){
                return false;
            }
        }
        return true;
    }
}
