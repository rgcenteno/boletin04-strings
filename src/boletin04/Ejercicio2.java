/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio2 {
    
    public static void esPalindromoProgram(){
        System.out.println("Por favor inserte la string a analizar");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        String texto = teclado.nextLine();
        System.out.printf("La palabra \"%s\" es palíndromo: %b\n", texto, esPalindromo(texto));
    }
    
    private static boolean esPalindromo(String texto){
        int ultimaPos = texto.length() - 1;
        for(int i = 0; i < (ultimaPos - i); i++){
            if(texto.charAt(i) != texto.charAt(ultimaPos - i)){
                return false;
            }
        }
        return true;
    }
    
    private static boolean esPalidromo2(String texto){
        String aux = "";
        for(int i = texto.length() - 1; i >= 0; i--){
            aux += texto.charAt(i);
        }
        return aux.equals(texto);
    }
    
    private static boolean esPalindromo3(String texto){        
        return new StringBuilder(texto).reverse().toString().equals(texto);       
    }

}
