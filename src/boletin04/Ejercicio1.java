/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin04;

import java.util.Scanner;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio1 {
    
    public static void contieneVocalesProgram(){
        System.out.println("Por favor inserte una cadena");
        Scanner teclado = new Scanner(System.in);
        String texto = teclado.nextLine();
        System.out.printf("El texto \"%s\" contiene todas las vocales: %b\n", texto, contieneTodasVocales(texto));
    }
    
    private static boolean contieneTodasVocales(String text){
        text = text.toLowerCase();
        boolean a = false, e = false, i = false, o = false, u = false;
        for(int x = 0; x < text.length(); x++){
            char actual = text.charAt(x);
            switch(actual){
                case 'a':
                    a = true;
                    break;
                case 'e':
                    e = true;
                    break;
                case 'i':
                    i = true;
                    break;
                case 'o':
                    o = true;
                    break;
                case 'u':
                    u = true;
                    break;
            }
        }
        return a && e && i && o && u;
    }
    
    private static boolean contieneTodasVocales2(String text){      
        text = text.toLowerCase();
        return text.contains("a") && text.contains("e") && text.contains("i") && text.contains("o") && text.contains("u");
    }
    
}
